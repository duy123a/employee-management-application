const fs = require("fs");
var Thu_muc_Du_lieu = "..\\Du_lieu";
var Thu_muc_Nhan_vien = Thu_muc_Du_lieu + "\\Nhan_vien";
var Thu_muc_HTML = Thu_muc_Du_lieu +"\\HTML";
class xuly{
    // Xử lý giao diện
    Doc_Khung_HTML(){
        var filePath = Thu_muc_HTML + "\\Khung.html";
        var chuoi_html = fs.readFileSync(filePath, 'utf-8');
        return chuoi_html;
    }
    Doc_Danh_sach_Nhan_vien(){
        var list = [];
        var list_name = fs.readdirSync(Thu_muc_Nhan_vien);
        list_name.forEach(name =>{
            var filePath = `${Thu_muc_Nhan_vien}\\${name}`;
            var string_json  = fs.readFileSync(filePath, 'utf-8');
            var staff = JSON.parse(string_json);
            list.push(staff);
        })
        return list;
    }
    Doc_Cong_ty(){
        var filePath = `${Thu_muc_Du_lieu}\\Cong_ty\\Cong_ty.json`;
        var string_json  = fs.readFileSync(filePath, 'utf-8');
        var result = JSON.parse(string_json);
        return result;
    }
    Ghi_Nhan_vien(Nhan_vien){
        var result= "";
        var filePath = `${Thu_muc_Nhan_vien}\\${Nhan_vien.Ma_so}.json`;
        var string_json = JSON.stringify(Nhan_vien,null,"\t");
        fs.writeFileSync(filePath,string_json);
        return result;
    }
    Ghi_Hinh_Nhan_vien(Nhan_vien, Hinh){
        var result= "";
        var filePath = `..\\Media\\${Nhan_vien.Ma_so}.png`;
        fs.writeFileSync(filePath,Hinh);
        return result;
    }
    // Xử lý thể hiện
    Tao_chuoi_HTML_Dang_nhap(username, pw, notice=''){
        var chuoi_html = `<form action="/Dang_nhap" method="post">
            <div class="alert" style="height:10px">
                Đăng nhập
            </div>
            <div class="alert" style="height:30px">
                <input name="TH_Ten_Dang_nhap" required="required" value='${username}' spellcheck="false" autocomplete="off">
            </div>
            <div class="alert" style="height:30px">
                <input name="TH_Mat_khau" required="required" value='${pw}' spellcheck="false" autocomplete="off">
            </div>
            <div class="alert" style="height:30px">
                <button class="btn btn-danger" type="submit">Đồng ý</button>
            </div>
            <br>
            <div>${notice}</div>
        </form>`
        return chuoi_html;
    }
    Tao_chuoi_HTML_Thuc_don_Nhan_vien(Nhan_vien){
        var Chuoi_Thuc_don=`<div>
            <form action='/Chon_Cap_nhat_Dien_thoai' method='post' class='btn'>
                <button type='submit' class='btn btn-primary'>Cập nhật điện thoại</button>
            </form>
            <form action='/Chon_Cap_nhat_Dia_chi' method='post' class='btn'>
                <button type='submit' class='btn btn-primary'>Cập nhật địa chỉ</button>
            </form>
            <form action='/Chon_Cap_nhat_Hinh' method='post' class='btn'>
                <button type='submit' class='btn btn-primary'>Cập nhật hình</button>
            </form>
            <form action='/Chon_Bo_sung_Ngoai_ngu' method='post' class='btn'>
                <button type='submit' class='btn btn-primary'>Bổ sung ngoại ngữ</button>
            </form>
        </div>`;
        return Chuoi_Thuc_don;
    }
    Tao_chuoi_String_Danh_sach_Ngoai_ngu(Danh_sach_Ngoai_ngu){
        var string = "";
        Danh_sach_Ngoai_ngu.forEach(function(x){
            string = string + x.Ten + " ";
        })
        return string;
    }
    Tao_chuoi_HTML_Nhan_vien(Nhan_vien){
        var Chuoi_Danh_sach_Ngoai_ngu = this.Tao_chuoi_String_Danh_sach_Ngoai_ngu(Nhan_vien.Danh_sach_Ngoai_ngu);
        var Ngay_sinh = new Date(Nhan_vien.Ngay_sinh);
        var Chuoi_HTML = "";
        var Chuoi_Hinh = `<img src='/Media/${Nhan_vien.Ma_so}.png' style='width:60px;height:60px'>`
        var Chuoi_Thong_tin = `<div class='btn' style='text-align:left'>
            <p>Họ tên: ${Nhan_vien.Ho_ten} - Giới tính: ${Nhan_vien.Gioi_tinh}</p>
            <p>CMND: ${Nhan_vien.CMND} - Ngày sinh: ${Ngay_sinh.toLocaleDateString("en-US")} - Mức lương: ${Nhan_vien.Muc_luong}</p>
            <p>Điện thoại: ${Nhan_vien.Dien_thoai} - Mail: ${Nhan_vien.Mail}</p>
            <p>Địa chỉ: ${Nhan_vien.Dia_chi} - Đơn vị: ${Nhan_vien.Don_vi.Ten}</p>
            <p>Khả năng ngoại ngữ: ${Chuoi_Danh_sach_Ngoai_ngu}</p>
        </div>`
        Chuoi_HTML = Chuoi_Thong_tin + Chuoi_Hinh;
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Cap_nhat_Dien_thoai(Nhan_vien){
        var Chuoi_HTML=`<form action="/Cap_nhat_Dien_thoai" method="post">
            <div class="alert" style="height:10px">
                Cập nhật điện thoại
            </div>
            <div class="alert" style="height:40px">
                <input type="text" name="TH_Dien_thoai" required="required" value="${Nhan_vien.Dien_thoai}">
            </div>
            <div class="alert" style="height:30px">
                <button class="btn btn-danger" type="submit">Đồng ý</button>
            </div>
        </form>`
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Cap_nhat_Dia_chi(Nhan_vien){
        var Chuoi_HTML=`<form action="/Cap_nhat_Dia_chi" method="post">
            <div class="alert" style="height:10px">
                Cập nhật địa chỉ
            </div>
            <div class="alert" style="height:40px">
                <textarea name="TH_Dia_chi" required="required" cols="25" rows="2">${Nhan_vien.Dia_chi}</textarea>
            </div>
            <div class="alert" style="height:30px">
                <button class="btn btn-danger" type="submit">Đồng ý</button>
            </div>
        </form>`
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Cap_nhat_Hinh(Nhan_vien){
        var Chuoi_HTML=`<form action="/Cap_nhat_Hinh" method="post" enctype="multipart/form-data">
            <div class="alert" style="height:10px">
                Cập nhật hình
            </div>
            <div class="alert" style="height:40px">
                <input type="file" name="TH_Hinh" accept="image/png">
            </div>
            <div class="alert" style="height:30px">
                <button class="btn btn-danger" type="submit">Đồng ý</button>
            </div>
        </form>`
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Bo_sung_Ngoai_ngu(Nhan_vien, Danh_sach_Ngoai_ngu){
        var Danh_sach_Bo_sung = [];
        Danh_sach_Ngoai_ngu.forEach(function(Ngoai_ngu){
            if(! Nhan_vien.Danh_sach_Ngoai_ngu.find(x=>x.Ma_so==Ngoai_ngu.Ma_so)){
                Danh_sach_Bo_sung.push(Ngoai_ngu);
            }
        })
        var Chuoi_HTML = "";
        Danh_sach_Bo_sung.forEach(function(Ngoai_ngu){
            Chuoi_HTML += `<form action="Bo_sung_Ngoai_ngu" method="post" class="btn">
                <input name="TH_Ma_so_Ngoai_ngu" type="hidden" value="${Ngoai_ngu.Ma_so}">
                <button class="btn btn-primary" type="submit">${Ngoai_ngu.Ten}</button>
            </form>`
        })
        return Chuoi_HTML;
    }
}

module.exports= new xuly();