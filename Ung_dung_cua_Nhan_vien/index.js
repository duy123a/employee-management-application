// Khai báo sử dụng thư viện hàm
const express = require("express");
const session = require("express-session");
const fileupload = require("express-fileupload");
const Xu_ly = require("./xuly");

// Khai báo cấu hình và ứng dụng
var app = express();
app.use(session({secret: '123456789'}));
app.use(fileupload());
app.use(express.urlencoded({extended: false}));
app.use("/Media", express.static("../Media"));
app.listen(3000);

// Biến cố khi người dùng khởi động, đăng nhập
app.get("/", XL_Khoi_dong);
app.post("/Dang_nhap", XL_Dang_nhap);
// Biến cố khi người dùng chọn chức năng
app.post("/Chon_Cap_nhat_Dien_thoai", XL_Chon_Cap_nhat_Dien_thoai);
app.post("/Chon_Cap_nhat_Dia_chi", XL_Chon_Cap_nhat_Dia_chi);
app.post("/Chon_Cap_nhat_Hinh", XL_Chon_Cap_nhat_Hinh);
app.post("/Chon_Bo_sung_Ngoai_ngu", XL_Chon_Bo_sung_Ngoai_ngu);
// Biến cố khi người dùng yêu cầu thực hiện chức năng
app.post("/Cap_nhat_Dien_thoai", XL_Cap_nhat_Dien_thoai);
app.post("/Cap_nhat_Dia_chi", XL_Cap_nhat_Dia_chi);
app.post("/Cap_nhat_Hinh", XL_Cap_nhat_Hinh);
app.post("/Bo_sung_Ngoai_ngu", XL_Bo_sung_Ngoai_ngu);

// Hàm xử lý biến cố
function XL_Khoi_dong(req, res){
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = Xu_ly.Tao_chuoi_HTML_Dang_nhap("NV_1","NV_1");
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Dang_nhap(req, res){
    var staff_list = Xu_ly.Doc_Danh_sach_Nhan_vien();
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var username = req.body.TH_Ten_Dang_nhap;
    var password = req.body.TH_Mat_khau;

    var chuoi_html = "";
    var staff = staff_list.find(function(x){
        return x.Ten_Dang_nhap == username && x.Mat_khau == password;
    })
    if (staff != null){
        req.session.staff = staff;
        chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Nhan_vien(staff) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
        chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    } else {
        chuoi_html = Xu_ly.Tao_chuoi_HTML_Dang_nhap("","","Đăng nhập không hợp lệ");
        chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    }
    res.send(chuoi_html);
}

function XL_Chon_Cap_nhat_Dien_thoai(req, res){
    var staff = req.session.staff;
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = "";
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Nhan_vien(staff) + Xu_ly.Tao_chuoi_HTML_Cap_nhat_Dien_thoai(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Chon_Cap_nhat_Dia_chi(req, res){
    var staff = req.session.staff;
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = "";
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Nhan_vien(staff) + Xu_ly.Tao_chuoi_HTML_Cap_nhat_Dia_chi(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Chon_Cap_nhat_Hinh(req, res){
    var staff = req.session.staff;
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = "";
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Nhan_vien(staff) + Xu_ly.Tao_chuoi_HTML_Cap_nhat_Hinh(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Chon_Bo_sung_Ngoai_ngu(req, res){
    var staff = req.session.staff;
    var Cong_ty = Xu_ly.Doc_Cong_ty();
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = "";
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Nhan_vien(staff) + Xu_ly.Tao_chuoi_HTML_Bo_sung_Ngoai_ngu(staff, Cong_ty.Danh_sach_Ngoai_ngu);
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Cap_nhat_Dien_thoai(req, res){
    var staff = req.session.staff;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Dien_thoai = req.body.TH_Dien_thoai;
    staff.Dien_thoai = Dien_thoai;
    Xu_ly.Ghi_Nhan_vien(staff);
    req.session.staff = staff;
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Nhan_vien(staff) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}

function XL_Cap_nhat_Dia_chi(req, res){
    var staff = req.session.staff;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Dia_chi = req.body.TH_Dia_chi;
    staff.Dia_chi = Dia_chi;
    Xu_ly.Ghi_Nhan_vien(staff);
    req.session.staff = staff;
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Nhan_vien(staff) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}

function XL_Cap_nhat_Hinh(req, res){
    var staff = req.session.staff;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Hinh = req.files.TH_Hinh.data;
    Xu_ly.Ghi_Hinh_Nhan_vien(staff, Hinh);
    req.session.staff = staff;
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Nhan_vien(staff) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}

function XL_Bo_sung_Ngoai_ngu(req, res){
    var staff = req.session.staff;
    var Cong_ty = Xu_ly.Doc_Cong_ty();
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Ma_so_Ngoai_ngu = req.body.TH_Ma_so_Ngoai_ngu;
    var Ngoai_ngu = Cong_ty.Danh_sach_Ngoai_ngu.find(function(x){
        return x.Ma_so == Ma_so_Ngoai_ngu;
    })
    staff.Danh_sach_Ngoai_ngu.push(Ngoai_ngu);
    Xu_ly.Ghi_Nhan_vien(staff);
    req.session.staff = staff;
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Nhan_vien(staff) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}