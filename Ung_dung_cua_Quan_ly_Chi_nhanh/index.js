// Khai báo sử dụng thư viện hàm
const express = require("express");
const session = require("express-session");
const fileupload = require("express-fileupload");
const Xu_ly = require("./xuly");

// Khai báo cấu hình và ứng dụng
var app = express();
app.use(session({secret: '123456789'}));
app.use(fileupload());
app.use(express.urlencoded({extended: false}));
app.use("/Media", express.static("../Media"));
app.listen(3001);

// Biến cố khi người dùng khởi động, đăng nhập
app.get("/", XL_Khoi_dong);
app.post("/Dang_nhap", XL_Dang_nhap);
// Biến cố khi người dùng chọn chức năng
app.post("/Chon_Tra_cuu", XL_Chon_Tra_cuu);
app.post("/Chon_Cap_nhat_Dien_thoai", XL_Chon_Cap_nhat_Dien_thoai);
app.post("/Chon_Cap_nhat_Dia_chi", XL_Chon_Cap_nhat_Dia_chi);
app.post("/Chon_Cap_nhat_Hinh", XL_Chon_Cap_nhat_Hinh);
// app.post("/Chon_Bo_sung_Ngoai_ngu", XL_Chon_Bo_sung_Ngoai_ngu);
app.post("/Chon_Chuyen_Don_vi", XL_Chon_Chuyen_Don_vi);
// Biến cố khi người dùng yêu cầu thực hiện chức năng
app.post("/Tra_cuu", XL_Tra_cuu);
app.post("/Bao_cao_Don_vi", XL_Bao_cao_Don_vi);
app.post("/Bao_cao_Ngoai_ngu", XL_Bao_cao_Ngoai_ngu);
app.post("/Cap_nhat_Dien_thoai", XL_Cap_nhat_Dien_thoai);
app.post("/Cap_nhat_Dia_chi", XL_Cap_nhat_Dia_chi);
app.post("/Cap_nhat_Hinh", XL_Cap_nhat_Hinh);
// app.post("/Bo_sung_Ngoai_ngu", XL_Bo_sung_Ngoai_ngu);
app.post("/Chuyen_Don_vi", XL_Chuyen_Don_vi);

// Hàm xử lý biến cố
function XL_Khoi_dong(req, res){
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = Xu_ly.Tao_chuoi_HTML_Dang_nhap("QLCN_2","QLCN_2");
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Dang_nhap(req, res){
    var manager_list = Xu_ly.Doc_Danh_sach_Quan_ly_Chi_nhanh();
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var username = req.body.TH_Ten_Dang_nhap;
    var password = req.body.TH_Mat_khau;

    var chuoi_html = "";
    var manager = manager_list.find(function(x){
        return x.Ten_Dang_nhap == username && x.Mat_khau == password;
    })
    if (manager != null){
        req.session.manager = manager;
        chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager);
        chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    } else {
        chuoi_html = Xu_ly.Tao_chuoi_HTML_Dang_nhap("","","Đăng nhập không hợp lệ");
        chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    }
    res.send(chuoi_html);
}

function XL_Chon_Tra_cuu(req, res){
    var manager = req.session.manager;
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Tra_cuu();
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Chon_Cap_nhat_Dien_thoai(req, res){
    var manager = req.session.manager;
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = "";
    var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Cap_nhat_Dien_thoai(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Chon_Cap_nhat_Dia_chi(req, res){
    var manager = req.session.manager;
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = "";
    var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Cap_nhat_Dia_chi(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Chon_Cap_nhat_Hinh(req, res){
    var manager = req.session.manager;
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var chuoi_html = "";
    var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Cap_nhat_Hinh(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

// function XL_Chon_Bo_sung_Ngoai_ngu(req, res){
//     var manager = req.session.manager;
//     var khung_html = Xu_ly.Doc_Khung_HTML();
//     var chuoi_html = "";
//     var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
//     var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
//     var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
//     var Cong_ty = Xu_ly.Doc_Cong_ty();
//     chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Bo_sung_Ngoai_ngu(staff, Cong_ty.Danh_sach_Ngoai_ngu);
//     chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
//     res.send(chuoi_html);
// }

function XL_Chon_Chuyen_Don_vi(req, res){
    var manager = req.session.manager;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Cong_ty = Xu_ly.Doc_Cong_ty();
    var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Chuyen_Don_vi(staff, Cong_ty.Danh_sach_Don_vi);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}

function XL_Tra_cuu(req, res){
    var manager = req.session.manager;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Chuoi_tra_cuu = req.body.TH_Chuoi_Tra_cuu;
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var Danh_sach_Nhan_vien_Xem = Xu_ly.Tra_cuu_Nhan_vien(Danh_sach_Nhan_vien, Chuoi_tra_cuu);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Danh_sach_Nhan_vien(Danh_sach_Nhan_vien_Xem);
    chuoi_html = khung_html.replace("Chuoi_HTML", chuoi_html);
    res.send(chuoi_html);
}

function XL_Bao_cao_Don_vi(req, res){
    var manager = req.session.manager;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Cong_ty = Xu_ly.Doc_Cong_ty();
    var Danh_sach_Don_vi = Cong_ty.Danh_sach_Don_vi.filter(x=>x.Chi_nhanh.Ma_so==manager.Chi_nhanh.Ma_so);
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var Bao_cao = Xu_ly.Lap_Bao_cao_Don_vi(Danh_sach_Don_vi, Danh_sach_Nhan_vien);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Bao_cao_Don_vi(Bao_cao);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}

function XL_Bao_cao_Ngoai_ngu(req, res){
    var manager = req.session.manager;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Cong_ty = Xu_ly.Doc_Cong_ty();
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var Bao_cao = Xu_ly.Lap_Bao_cao_Ngoai_ngu(Cong_ty.Danh_sach_Ngoai_ngu, Danh_sach_Nhan_vien);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Bao_cao_Ngoai_ngu(Bao_cao);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}

function XL_Cap_nhat_Dien_thoai(req, res){
    var manager = req.session.manager;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
    var Dien_thoai = req.body.TH_Dien_thoai;
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
    staff.Dien_thoai = Dien_thoai;
    Xu_ly.Ghi_Nhan_vien(staff);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}

function XL_Cap_nhat_Dia_chi(req, res){
    var manager = req.session.manager;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
    var Dia_chi = req.body.TH_Dia_chi;
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
    staff.Dia_chi = Dia_chi;
    Xu_ly.Ghi_Nhan_vien(staff);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}

function XL_Cap_nhat_Hinh(req, res){
    var manager = req.session.manager;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
    var Hinh = req.files.TH_Hinh.data;
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
    Xu_ly.Ghi_Hinh_Nhan_vien(staff, Hinh);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}

// function XL_Bo_sung_Ngoai_ngu(req, res){
//     var manager = req.session.manager;
//     var Cong_ty = Xu_ly.Doc_Cong_ty();
//     var chuoi_html = "";
//     var khung_html = Xu_ly.Doc_Khung_HTML();
//     var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
//     var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
//     var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
//     var Ma_so_Ngoai_ngu = req.body.TH_Ma_so_Ngoai_ngu;
//     var Ngoai_ngu = Cong_ty.Danh_sach_Ngoai_ngu.find(function(x){
//         return x.Ma_so == Ma_so_Ngoai_ngu;
//     })
//     staff.Danh_sach_Ngoai_ngu.push(Ngoai_ngu);
//     Xu_ly.Ghi_Nhan_vien(staff);
//     chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
//     chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
//     res.send(chuoi_html);
// }

function XL_Chuyen_Don_vi(req, res){
    var manager = req.session.manager;
    var chuoi_html = "";
    var khung_html = Xu_ly.Doc_Khung_HTML();
    var Ma_so_Nhan_vien = req.body.TH_Ma_so_Nhan_vien;
    var Ma_so_Don_vi = req.body.TH_Ma_so_Don_vi;
    var Cong_ty = Xu_ly.Doc_Cong_ty();
    var Danh_sach_Nhan_vien = Xu_ly.Doc_Danh_sach_Nhan_vien(manager);
    var staff = Danh_sach_Nhan_vien.find(x => x.Ma_so == Ma_so_Nhan_vien);
    staff.Don_vi = Cong_ty.Danh_sach_Don_vi.find(x=>x.Ma_so == Ma_so_Don_vi);
    Xu_ly.Ghi_Nhan_vien(staff);
    chuoi_html = Xu_ly.Tao_chuoi_HTML_Thuc_don_Quan_ly(manager) + Xu_ly.Tao_chuoi_HTML_Nhan_vien(staff);
    chuoi_html = khung_html.replace("Chuoi_HTML",chuoi_html);
    res.send(chuoi_html);
}