const fs = require("fs");
var Thu_muc_Du_lieu = "..\\Du_lieu";
var Thu_muc_Nhan_vien = Thu_muc_Du_lieu + "\\Nhan_vien";
var Thu_muc_Quan_ly = Thu_muc_Du_lieu + "\\Quan_ly_Chi_nhanh";
var Thu_muc_HTML = Thu_muc_Du_lieu +"\\HTML";
class xuly{
    // Xử lý giao diện
    Doc_Khung_HTML(){
        var filePath = Thu_muc_HTML + "\\Khung.html";
        var chuoi_html = fs.readFileSync(filePath, 'utf-8');
        return chuoi_html;
    }
    Doc_Danh_sach_Quan_ly_Chi_nhanh(){
        var list = [];
        var list_name = fs.readdirSync(Thu_muc_Quan_ly);
        list_name.forEach(name =>{
            var filePath = `${Thu_muc_Quan_ly}\\${name}`;
            var string_json  = fs.readFileSync(filePath, 'utf-8');
            var manager = JSON.parse(string_json);
            list.push(manager);
        })
        return list;
    }
    Doc_Danh_sach_Nhan_vien(Quan_ly_Chi_nhanh){
        var list = [];
        var list_name = fs.readdirSync(Thu_muc_Nhan_vien);
        list_name.forEach(name =>{
            var filePath = `${Thu_muc_Nhan_vien}\\${name}`;
            var string_json  = fs.readFileSync(filePath, 'utf-8');
            var staff = JSON.parse(string_json);
            if(staff.Don_vi.Chi_nhanh.Ma_so == Quan_ly_Chi_nhanh.Chi_nhanh.Ma_so){
                list.push(staff);
            }
        })
        return list;
    }
    Doc_Cong_ty(){
        var filePath = `${Thu_muc_Du_lieu}\\Cong_ty\\Cong_ty.json`;
        var string_json  = fs.readFileSync(filePath, 'utf-8');
        var result = JSON.parse(string_json);
        return result;
    }
    Ghi_Nhan_vien(Nhan_vien){
        var result= "";
        var filePath = `${Thu_muc_Nhan_vien}\\${Nhan_vien.Ma_so}.json`;
        var string_json = JSON.stringify(Nhan_vien,null,"\t");
        fs.writeFileSync(filePath,string_json);
        return result;
    }
    Ghi_Hinh_Nhan_vien(Nhan_vien, Hinh){
        var result= "";
        var filePath = `..\\Media\\${Nhan_vien.Ma_so}.png`;
        fs.writeFileSync(filePath,Hinh);
        return result;
    }
    // Xử lý nghiệp vụ
    Tra_cuu_Nhan_vien(Danh_sach, Chuoi_tra_cuu){
        var list_result = [];
        Chuoi_tra_cuu = Chuoi_tra_cuu.toUpperCase();
        Danh_sach.forEach(Nhan_vien =>{
            var Ho_ten = Nhan_vien.Ho_ten.toUpperCase();
            var Ma_so = Nhan_vien.Ma_so.toUpperCase();
            var Ten_Don_vi = Nhan_vien.Don_vi.Ten.toUpperCase();
            var Ten_Chi_nhanh = Nhan_vien.Don_vi.Chi_nhanh.Ten.toUpperCase();
            var Thoa_Dieu_kien_Tra_cuu = Ho_ten.includes(Chuoi_tra_cuu) || Ma_so.includes(Chuoi_tra_cuu) || 
                                        Ten_Don_vi.includes(Chuoi_tra_cuu) || Ten_Chi_nhanh.includes(Chuoi_tra_cuu);
            if(Thoa_Dieu_kien_Tra_cuu){
                list_result.push(Nhan_vien);
            }
        })
        return list_result;
    }
    Lap_Bao_cao_Don_vi(Danh_sach_Don_vi, Danh_sach_Nhan_vien){
        var Bao_cao = {};
        Bao_cao.Tieu_de = "Thống kê số nhân viên theo đơn vị";
        Bao_cao.Danh_sach_Chi_tiet=[];
        Danh_sach_Don_vi.forEach(Don_vi=>{
            var Chi_tiet = {};
            Chi_tiet.Ten_Don_vi = Don_vi.Ten;
            var So_Nhan_vien = 0;
            Danh_sach_Nhan_vien.forEach(Nhan_vien=>{
                if(Nhan_vien.Don_vi.Ma_so == Don_vi.Ma_so){
                    So_Nhan_vien ++;
                }
            })
            Chi_tiet.So_Nhan_vien = So_Nhan_vien;
            var Ty_le = (Chi_tiet.So_Nhan_vien*100.0)/Danh_sach_Nhan_vien.length;
            Chi_tiet.Ty_le = Ty_le.toFixed(2);
            Bao_cao.Danh_sach_Chi_tiet.push(Chi_tiet);
        })
        return Bao_cao;
    }
    Lap_Bao_cao_Ngoai_ngu(Danh_sach_Ngoai_ngu,Danh_sach_Nhan_vien){
        var Bao_cao = {};
        Bao_cao.Tieu_de = "Thống kê số nhân viên theo ngoại ngữ";
        Bao_cao.Danh_sach_Chi_tiet=[];
        Danh_sach_Ngoai_ngu.forEach(Ngoai_ngu=>{
            var Chi_tiet = {};
            Chi_tiet.Ten_Ngoai_ngu = Ngoai_ngu.Ten;
            var So_Nhan_vien = 0;
            Danh_sach_Nhan_vien.forEach(Nhan_vien=>{
                if(Nhan_vien.Danh_sach_Ngoai_ngu.map(x=>x.Ma_so).join("_").includes(Ngoai_ngu.Ma_so)){
                    So_Nhan_vien ++;
                }
            })
            Chi_tiet.So_Nhan_vien = So_Nhan_vien;
            var Ty_le = (Chi_tiet.So_Nhan_vien*100.0)/Danh_sach_Nhan_vien.length;
            Chi_tiet.Ty_le = Ty_le.toFixed(2);
            Bao_cao.Danh_sach_Chi_tiet.push(Chi_tiet);
        })
        return Bao_cao;
    }
    // Xử lý thể hiện
    Tao_chuoi_HTML_Dang_nhap(username, pw, notice=''){
        var chuoi_html = `<form action="/Dang_nhap" method="post">
            <div class="alert" style="height:10px">
                Đăng nhập
            </div>
            <div class="alert" style="height:30px">
                <input name="TH_Ten_Dang_nhap" required="required" value='${username}' spellcheck="false" autocomplete="off">
            </div>
            <div class="alert" style="height:30px">
                <input name="TH_Mat_khau" required="required" value='${pw}' spellcheck="false" autocomplete="off">
            </div>
            <div class="alert" style="height:30px">
                <button class="btn btn-danger" type="submit">Đồng ý</button>
            </div>
            <br>
            <div>${notice}</div>
        </form>`
        return chuoi_html;
    }
    Tao_chuoi_HTML_Thuc_don_Nhan_vien(Nhan_vien){
        var Chuoi_Thuc_don=`<div>
            <form action='/Chon_Cap_nhat_Dien_thoai' method='post' class='btn'>
                <input type="hidden" name="TH_Ma_so_Nhan_vien" value="${Nhan_vien.Ma_so}">
                <button type='submit' class='btn btn-primary'>Cập nhật điện thoại</button>
            </form>
            <form action='/Chon_Cap_nhat_Dia_chi' method='post' class='btn'>
                <input type="hidden" name="TH_Ma_so_Nhan_vien" value="${Nhan_vien.Ma_so}">
                <button type='submit' class='btn btn-primary'>Cập nhật địa chỉ</button>
            </form>
            <form action='/Chon_Cap_nhat_Hinh' method='post' class='btn'>
                <input type="hidden" name="TH_Ma_so_Nhan_vien" value="${Nhan_vien.Ma_so}">
                <button type='submit' class='btn btn-primary'>Cập nhật hình</button>
            </form>
            <form action='/Chon_Chuyen_Don_vi' method='post' class='btn'>
                <input type="hidden" name="TH_Ma_so_Nhan_vien" value="${Nhan_vien.Ma_so}">
                <button type='submit' class='btn btn-primary'>Chuyển đơn vị</button>
            </form>
        </div>`;
        return Chuoi_Thuc_don;
    }
    Tao_chuoi_HTML_Thuc_don_Quan_ly(Quan_ly_Chi_nhanh){
        var Chuoi_Thuc_don=`<div>
            <form action='/Chon_Tra_cuu' method='post' class='btn'>
                <button type='submit' class='btn btn-primary'>Tra cứu nhân viên</button>
            </form>
            <form action='/Bao_cao_Don_vi' method='post' class='btn'>
                <button type='submit' class='btn btn-primary'>Thống kê đơn vị</button>
            </form>
            <form action='/Bao_cao_Ngoai_ngu' method='post' class='btn'>
                <button type='submit' class='btn btn-primary'>Thống kê ngoại ngữ</button>
            </form>
        </div>`;
        return Chuoi_Thuc_don;
    }
    Tao_chuoi_HTML_Tra_cuu(){
        var Chuoi_HTML = `<div style="background-color:gray;main:10px">
            <form action="/Tra_cuu" method="post" class="btn">
                <input name="TH_Chuoi_Tra_cuu" value="" spellcheck="false">
            </form>
        </div>`
        return Chuoi_HTML;
    }
    Tao_chuoi_String_Danh_sach_Ngoai_ngu(Danh_sach_Ngoai_ngu){
        var string = "";
        Danh_sach_Ngoai_ngu.forEach(function(x){
            string = string + x.Ten + " ";
        })
        return string;
    }
    Tao_chuoi_HTML_Danh_sach_Nhan_vien(Danh_sach){
        var Chuoi_HTML = "";
        Danh_sach.forEach(Nhan_vien => {
            Chuoi_HTML += this.Tao_chuoi_HTML_Nhan_vien(Nhan_vien);
        })
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Nhan_vien(Nhan_vien){
        var Chuoi_Danh_sach_Ngoai_ngu = this.Tao_chuoi_String_Danh_sach_Ngoai_ngu(Nhan_vien.Danh_sach_Ngoai_ngu);
        var Ngay_sinh = new Date(Nhan_vien.Ngay_sinh);
        var Chuoi_HTML = "";
        var Chuoi_Menu = this.Tao_chuoi_HTML_Thuc_don_Nhan_vien(Nhan_vien);
        var Chuoi_Hinh = `<img src='/Media/${Nhan_vien.Ma_so}.png' style='width:60px;height:60px'>`
        var Chuoi_Thong_tin = `<div class='btn' style='text-align:left'>
            <p>Họ tên: ${Nhan_vien.Ho_ten} - Giới tính: ${Nhan_vien.Gioi_tinh}</p>
            <p>CMND: ${Nhan_vien.CMND} - Ngày sinh: ${Ngay_sinh.toLocaleDateString("en-US")} - Mức lương: ${Nhan_vien.Muc_luong}</p>
            <p>Điện thoại: ${Nhan_vien.Dien_thoai} - Mail: ${Nhan_vien.Mail}</p>
            <p>Địa chỉ: ${Nhan_vien.Dia_chi} - Đơn vị: ${Nhan_vien.Don_vi.Ten}</p>
            <p>Khả năng ngoại ngữ: ${Chuoi_Danh_sach_Ngoai_ngu}</p>
        </div>`
        Chuoi_HTML = Chuoi_Menu + Chuoi_Thong_tin + Chuoi_Hinh;
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Cap_nhat_Dien_thoai(Nhan_vien){
        var Chuoi_HTML=`<form action="/Cap_nhat_Dien_thoai" method="post">
            <div class="alert" style="height:10px">
                Cập nhật điện thoại
            </div>
            <div class="alert" style="height:40px">
                <input type="hidden" name="TH_Ma_so_Nhan_vien" value="${Nhan_vien.Ma_so}">
                <input type="text" name="TH_Dien_thoai" required="required" value="${Nhan_vien.Dien_thoai}">
            </div>
            <div class="alert" style="height:30px">
                <button class="btn btn-danger" type="submit">Đồng ý</button>
            </div>
        </form>`
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Cap_nhat_Dia_chi(Nhan_vien){
        var Chuoi_HTML=`<form action="/Cap_nhat_Dia_chi" method="post">
            <div class="alert" style="height:10px">
                Cập nhật địa chỉ
            </div>
            <div class="alert" style="height:40px">
                <input type="hidden" name="TH_Ma_so_Nhan_vien" value="${Nhan_vien.Ma_so}"> 
                <textarea name="TH_Dia_chi" required="required" cols="25" rows="2">${Nhan_vien.Dia_chi}</textarea>
            </div>
            <div class="alert" style="height:30px">
                <button class="btn btn-danger" type="submit">Đồng ý</button>
            </div>
        </form>`
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Cap_nhat_Hinh(Nhan_vien){
        var Chuoi_HTML=`<form action="/Cap_nhat_Hinh" method="post" enctype="multipart/form-data">
            <div class="alert" style="height:10px">
                Cập nhật hình
            </div>
            <div class="alert" style="height:40px">
                <input type="hidden" name="TH_Ma_so_Nhan_vien" value="${Nhan_vien.Ma_so}">
                <input type="file" name="TH_Hinh" accept="image/png">
            </div>
            <div class="alert" style="height:30px">
                <button class="btn btn-danger" type="submit">Đồng ý</button>
            </div>
        </form>`
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Bo_sung_Ngoai_ngu(Nhan_vien, Danh_sach_Ngoai_ngu){
        var Danh_sach_Bo_sung = [];
        Danh_sach_Ngoai_ngu.forEach(function(Ngoai_ngu){
            if(! Nhan_vien.Danh_sach_Ngoai_ngu.find(x=>x.Ma_so==Ngoai_ngu.Ma_so)){
                Danh_sach_Bo_sung.push(Ngoai_ngu);
            }
        })
        var Chuoi_HTML = "";
        Danh_sach_Bo_sung.forEach(function(Ngoai_ngu){
            Chuoi_HTML += `<form action="Bo_sung_Ngoai_ngu" method="post" class="btn">
                <input type="hidden" name="TH_Ma_so_Nhan_vien" value="${Nhan_vien.Ma_so}">
                <input name="TH_Ma_so_Ngoai_ngu" type="hidden" value="${Ngoai_ngu.Ma_so}">
                <button class="btn btn-primary" type="submit">${Ngoai_ngu.Ten}</button>
            </form>`
        })
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Chuyen_Don_vi(Nhan_vien, Danh_sach_Don_vi){
        var Chuoi_HTML=`<div class="alert" style="height:10px">
            Chuyển đơn vị nhân viên ${Nhan_vien.Ho_ten}
        </div>`
        Danh_sach_Don_vi.forEach(Don_vi=>{
            if (Nhan_vien.Don_vi.Chi_nhanh.Ma_so == Don_vi.Chi_nhanh.Ma_so && Nhan_vien.Don_vi.Ma_so != Don_vi.Ma_so){
                Chuoi_HTML += `<form action="/Chuyen_Don_vi" method="post" class="btn">
                    <input type="hidden" name="TH_Ma_so_Nhan_vien" value="${Nhan_vien.Ma_so}">
                    <input type="hidden" name="TH_Ma_so_Don_vi" value="${Don_vi.Ma_so}">
                    <button class="btn btn-primary" type="submit">${Don_vi.Ten}</button>
                </form>`
            }
        })
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Bao_cao_Don_vi(Bao_cao){
        var Chuoi_HTML=`<div class="alert alert-info" style="margin:10px">${Bao_cao.Tieu_de}</div>`;
        Chuoi_HTML += `<div class="row" style="margin:10px">
            <div class="col-md-2 btn btn-info">Đơn vị</div>
            <div class="col-md-2 btn btn-info">Số nhân viên</div>
            <div class="col-md-2 btn btn-info">Tỷ lệ</div>
        </div>`
        Bao_cao.Danh_sach_Chi_tiet.forEach(Chi_tiet=>{
            Chuoi_HTML += `<div class="row" style="margin:10px">
                <div class="col-md-2">${Chi_tiet.Ten_Don_vi}</div>
                <div class="col-md-2">${Chi_tiet.So_Nhan_vien}</div>
                <div class="col-md-2">${Chi_tiet.Ty_le}</div>
            </div>`
        })
        return Chuoi_HTML;
    }
    Tao_chuoi_HTML_Bao_cao_Ngoai_ngu(Bao_cao){
        var Chuoi_HTML=`<div class="alert alert-info" style="margin:10px">${Bao_cao.Tieu_de}</div>`;
        Chuoi_HTML += `<div class="row" style="margin:10px">
            <div class="col-md-2 btn btn-info">Ngoại ngữ</div>
            <div class="col-md-2 btn btn-info">Số nhân viên</div>
            <div class="col-md-2 btn btn-info">Tỷ lệ</div>
        </div>`
        Bao_cao.Danh_sach_Chi_tiet.forEach(Chi_tiet=>{
            Chuoi_HTML += `<div class="row" style="margin:10px">
                <div class="col-md-2">${Chi_tiet.Ten_Ngoai_ngu}</div>
                <div class="col-md-2">${Chi_tiet.So_Nhan_vien}</div>
                <div class="col-md-2">${Chi_tiet.Ty_le}</div>
            </div>`
        })
        return Chuoi_HTML;
    }
}

module.exports= new xuly();